public class PercolationStats 
{
        private int T;
        
        private Percolation[] percolation;
        private double[] a;
        
        private final double CONFIDENCE = 1.96;

        
        public PercolationStats(int N, int T)
        {
        	if (N < 1 || T < 1) {
                throw new IllegalArgumentException("Invalid Input!");
            }
        	
        		this.T = T;
                percolation = new Percolation[T];
                a = new double[T];
        }

        public double mean()
        {
                return StdStats.mean(a);
        }
        public double stddev(){
                if (T == 1)
                {
                        return Double.NaN;
                }
                return StdStats.stddev(a);
        }
        public double confidenceLo()
        {
                return mean() - ((CONFIDENCE * stddev()) / Math.sqrt(T));
        }
        public double confidenceHi()
        {
                return mean() + ((CONFIDENCE * stddev()) / Math.sqrt(T));
        }
        
        public static void main(String[] args)
        {
//             In input = new In(args[0]);   
//             int N = input.readInt();
//             int T = input.readInt();
//                     
//             
//             PercolationStats percStat = new PercolationStats(N, T);
//             for (int a = 0; a < T; a++)
//             {
//                 percStat.percolation[a] = new Percolation(N);
//                 double total = 0;
//                 while (!percStat.percolation[a].percolates())
//                 {
//                         int i = StdRandom.uniform(N) + 1;
//                         int j = StdRandom.uniform(N) + 1;
//                         if (!percStat.percolation[a].isOpen(i, j))
//                         {
//                                 percStat.percolation[a].open(i, j);
//                                 total++;
//                         }
//                         
//                 }
//                 
//                 percStat.a[a] = total / (N * N);
//             }
//             System.out.println("mean = " + percStat.mean());
//             System.out.println("95% confidence interval = " + percStat.confidenceLo() + "," + percStat.confidenceHi());
//             System.out.println("stddev = " + percStat.stddev());
        }
}