public class Percolation {

	private final int OPEN;
	private final int FULL;
	private int n;
	private WeightedQuickUnionUF UF;
	private int[] openCells;
	private int Percolates;
	
	public Percolation(int n)
	{
		UF = new WeightedQuickUnionUF(n*n + 3); //2
		this.n = n;
		OPEN = 1;
		openCells = new int[n*n];
		FULL = n*n + 2;
	}
	
	public boolean isOpen(int row, int col) 
	{
		if (openCells[((row - 1) * n) + col - 1] == OPEN)
			return true;
		return false;
	}
	
	public void open(int i, int j) //row, col
	{
			int index = ((i - 1) * n) + j - 1;
			if (isFull(i, j))
			{
				return;
			}
			else
			{
				System.out.println("Begining of test");
				if (index < n) //Is it on the top row?
				{
					UF.union(index, FULL);
					if (i+1 < n && isOpen(i+1,j))//below it
						open(i+1,j);
					
				}
				else if (index - n >= 0 && this.isFull(i-1,j)) //above it
				{
					UF.union(index, index - n);
					if (index < n*n && index >= n*n - n) //is it on the bottom row
						Percolates = 1;
					if (i+1 < n && isOpen(i+1,j))//below it
						open(i+1,j);
					if (j+1 < n && isOpen(i,j+1))//right
						open(i,j+1);
					if (j-1 >= 0 && isOpen(i,j-1))//left
						open(i,j-1);
					
						
				}
				else if (index - 1 >= 0 && this.isFull(i,j-1)) //left
				{
					UF.union(index, index - 1);
					if (i+1 < n && isOpen(i+1,j))//below it
						open(i+1,j);
					if (j+1 < n && isOpen(i,j+1))//right
						open(i,j+1);
					if (j-1 >= 0 && isOpen(i,j-1))//left
						open(i,j-1);
					
				}
				else if (index + 1 >= 0 && this.isFull(i-1,j+1)) //right
				{
					UF.union(index, index + 1);
					if (i+1 < n && isOpen(i+1,j))//below it
						open(i+1,j);
					if (j+1 < n && isOpen(i,j+1))//right
						open(i,j+1);
					if (j-1 >= 0 && isOpen(i,j-1))//left
						open(i,j-1);
					
				}
				else
				{	
					if (isOpen(i,j))
						System.out.println("Cell was already open and did not have any adjcanent filled cells");
					else
						openCells[index] = 1;	
					
				}
			}
				
	}

	
	public boolean percolates() 
	{
		if (Percolates == 1)
			return true;
		else
			return false;
	}
	

	public boolean isFull(int row, int col) {
		if (UF.connected(((row - 1) * n) + col - 1, FULL))
			return true;
		else
			return false;
	}
	
}
